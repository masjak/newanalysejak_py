#from sys import exit
import math
import os
import os.path
import time
import sys


dirvisu =  '/home/pmarchan/VISU_ref/newanalysejak_py/'
#dirvisu =  './'
var=["AMR","r","rho","v","B","T","J","Xi","Plasma","disk","vtheta","none"]
varnamegp=["Level","log(r) (a.u.)","log({/Symbol r}) (g cm^{-3})","v (m.s^{-1})","log(B) (G)","log(T) (K)", "J (J/J_{max})","log({/Symbol x}) (s^{-1})","Plasma Beta","log({/Symbol r}) (g cm^{-3})","km.s^{-1}",""]
unit=["lvl","a.u.","g.cm^{-3}","km.s^{-1}","G","K","c.u.","s^{-1}","","g.cm^{-3}","km.s^{-1}",""]
isvarlog={}
isvarlog={"AMR": False,"r": True,"rho": True,"v": False,"B": True,"T": True, "J": True, "Xi": True, "Plasma": True, "disk": True , "vtheta": False , "": False} 
varvect=["v","B","J","fluxmom","none"]
plot=["histo","map"]
helplist={}
helplist={".plot": plot,".var": var,"r":''}


def generatemap(choice):
    print "TBD"

def help():
    print "Help !"
    print "List of plot choices : .plot"
    print "List of variables : .var"
    choice = raw_input("Press 'r' to restart, or any command for help >  ").strip()
    print helplist.get(choice)
    start()

def dead(why):
    print why, "Good job!"
    exit(0)

class Default:
    def __init__(self,var):
      minval={"AMR":7,  "r": 1.e-2, "B":1.e-5, "rho":1.e-18, "v":1.e-3, "T": 8   , "J": 1.e-3, "none": 0, "Xi": 1e-19,   "Plasma": 1e-3, "disk":1e-18, "vtheta":-2.0} 
      maxval={"AMR":15, "r": 1.e4,  "B":0.1,   "rho":1.e-10, "v":2.e0,  "T": 5000, "J": 1,     "none": 0, "Xi": 1e-16,   "Plasma": 1e3,  "disk":1e-10, "vtheta": 2.0}   
      self.m=float(minval.get(var))
      self.M=float(maxval.get(var))

class vector:
    def __init__(self,x,y,z):
        self.x=x/math.sqrt(x*x+y*y+z*z)
        print "x coordinate: %s" % self.x
        self.y=y/math.sqrt(x*x+y*y+z*z)
        print "y coordinate: %s" % self.y
        self.z=z/math.sqrt(x*x+y*y+z*z)
        print "z coordinate: %s" % self.z


######################################################              ######################################################
######################################################              ######################################################
######################################################      MAP     ######################################################
######################################################              ######################################################
######################################################              ######################################################
class Mapp:
    def __init__(self):
        print '\n --map not available atm... plotting an histogram instead --- actually no, in progress ! \n'
        print '\n'
        #H=Histogram()

        self.genesize()
        self.genezoom()
        self.genemap()
        self.genemax()

        ###############################    findcenter    ########################
        sinks = raw_input("Do you use sinks ? (y/n default is no.) \n>")
        if sinks == "y":
            self.sinks=True
            self.findcenter=False
        else:
            self.sinks=False
            self.findcenter=True 

        findcenter=raw_input("Find max density and use as center ? (y/n default is yes) \n>")
        if findcenter == "n":
            self.findcenter=False
        else:
            self.findcenter=True

        while True:
            output=raw_input("Process all files (all) or select an output (number). Last output by default. \n>")
            if output=="all":
                self.output="all"
                print("all outputs will be processed")
                break
            elif output=="":
                self.output="last"
                print("processing last output")
                break
            else:
                try:
                    self.output=int(output)
                    print("processing output_%.5d" % int(output))
                    break
                except ValueError:
                    print("enter a valid number, or 'all', or hit enter for last output...\n")

        ###############################    erase    ########################
        erase=raw_input("Erase existing plots ? (y/n default is yes) \n>")
        if erase == "n":
            self.erase=False
            #if self.rhotcenter == True:
                #self.erase=True
                #print('Override mandatory for tracking of central temperature ---- Overriding')
        else:
            self.erase=True

        #self.dirname="map"+self.color+"_vect"+self.vect
        self.dirname=self.color+self.vect
        self.dirvisu=dirvisu
        self.generategpmap()
        self.generateshmap()

    def generatevector(self):
        print("Enter the coordinates of the direction vector (it will renormalize by itself):\n")
        try:
            xx=float(raw_input("x ? \n>"))
            yy=float(raw_input("y ? \n>"))
            zz=float(raw_input("z ? \n>"))
            self.vecteur=vector(xx,yy,zz)
            return 
        except ValueError:
            print 'rate, essaie encore'
            self.generatevector()

    def genesize(self):
        print "Size generation"
        unitmap = "a.u."
        while True:
            size = raw_input("Size of the window (in %s) ? " % unitmap)
            try: 
                self.size = float(size)
                break
            except ValueError:
                print "%s Not a valid size, try again.\n" % size
                #self.genesize()
        self.unitmap = unitmap
        print("Window of %s (in %s)\n" % (self.size,self.unitmap))
        
    def genezoom(self):
        print "Resolution (number of points per window)"
        fenetredefault = 200
        while True:
            zoom = raw_input("Resolution ? (default is %s)" % fenetredefault)
            if zoom == '':
                self.zoom=fenetredefault
                break
            else:
                try: 
                    self.zoom = float(zoom)
                    break
                except ValueError:
                    print "%s Not a valid zoom, try again.\n" % zoom
                    #self.genesize()
        #print("Zooming in: 2^%s\n" % (self.zoom))
        
        waistdefault = 5   # 5 au above and below the diskplane + un single cell height if not enough refined.
        while True:
            waist = raw_input("Vertical average for the orthogonal view (in %s)? (default is waist=%s %s above and below the midplane)" % (self.unitmap,waistdefault,self.unitmap))
            if waist == '':
                self.waist=waistdefault
                break
            else:
                try: 
                    self.waist = float(waist)
                    break
                except ValueError:
                    print "%s Not a valid zoom, try again.\n" % waist
                    #self.genesize()
        self.waist=5
        print("Average over %s %s above and below the midplane\n" % (self.waist,self.unitmap))
    
    def genemap(self):
        print "Variables generation"
        #Colormap
        while True:
            try: 
                self.color = raw_input("colormap variable ? ")
                self.icolor = var.index(self.color)
                break
            except ValueError:
                print "%s Not in variables list, try again." % self.color
                print "Reminder of variables: ",var,"\n"
        self.unitcolor = unit[self.icolor]
        self.namecolor = varnamegp[self.icolor]
        #Vectorfiled
        while True:
            try: 
                self.vect = raw_input("vectorfield variable ? ")
                self.ivect = var.index(self.vect)
                wrongvar = varvect.index(self.vect)
                break
            except ValueError:
                print "%s Not in variables list, try again." % self.vect
                print "Reminder of variables: ",varvect,"\n"
        self.btheta = False
        if self.vect == "B":
            thetastring = u'\u0398'.encode('utf-8')
            btheta = raw_input("Vector field is by default |B_"+thetastring+"|/||B||. type 'n' to use ||B|| as color for the vector field.\n")
            if btheta != "n":
                self.btheta = True
        if self.vect == "J" or self.vect == "none":
            self.btheta = True
        self.unitvect = unit[self.ivect]
        self.namevect = varnamegp[self.ivect]


    def genemax(self):
        #colormap
        self.mincolor=Default(self.color).m
        self.maxcolor=Default(self.color).M
        try:
            self.mincolor = float(raw_input("Min value for %s ? Default is %s (in %s)\n>" % (self.color,self.mincolor,self.unitcolor) ))
        except ValueError:
            print 'Default used for minimum %s' % self.color
        try:
            self.maxcolor = float(raw_input("Max value for %s ? Default is %s (in %s)\n>" % (self.color,self.maxcolor,self.unitcolor) ))
        except ValueError:
            print 'Default value used for maximum %s' % self.color
        #vectorfield
        self.minvect=Default(self.vect).m
        self.maxvect=Default(self.vect).M
        if self.btheta == False:
            try:
                self.minvect = float(raw_input("Min value for %s ? Default is %s (in %s)\n>" % (self.vect,self.minvect,self.unitvect) ))
            except ValueError:
                print 'Default used for minimum %s' % self.vect
            try:
                self.maxvect = float(raw_input("Max value for %s ? Default is %s (in %s)\n>" % (self.vect,self.maxvect,self.unitvect) ))
            except ValueError:
                print 'Default value used for maximum %s' % self.vect
        return

    def generategpmap(self):
        print('Creating new gp files .... ') 
        name = self.dirname
        try:
            file = open('plandisk_'+self.dirname+'.gp','w')
            file.write("set view map\n")
            #file.write("set logscale cb\n")
            if self.color == "Plasma" or self.color == "vtheta":
              file.write('set palette model RGB defined (-0.5 "violet", -0.3 "blue", -0.05 "cyan", 0 "white", 0.05 "yellow", 0.3 "orange", 0.5 "red")\n')
            else:
              file.write('set palette rgbformulae 22,13,-31\n')
            file.write("\n")

            file.write('t=system("awk \'/t =/ \{print $3\}\' br.txt")\n')
            file.write('t=t+0.0\n')
            file.write('newlab=sprintf("time = %4.3f Kyears",t)\n')
            file.write('set label front newlab at screen 0.45, screen 0.98\n')
            file.write('\n')
#file.write("msink = `echo $(awk '{print $6}' center.dat)`\n")
#file.write("vx = `echo $(awk '{print $9}' center.dat)`\n")
#file.write("vy = `echo $(awk '{print $12}' center.dat)`\n")
#file.write("vz = `echo $(awk '{print $15}' center.dat)`\n")
#file.write("print msink\n")
#file.write("print vx, vy,vz\n")
            file.write("\n")
            #file.write("set xlabel 'log({/Symbol r}) (g cm^{-3})'\n")
            #file.write("set xlabel '"+self.namex+"'\n")
            file.write("set xlabel 'R ("+self.unitmap+")'\n")
            #file.write("set ylabel '"+self.namey+"'\n")
            file.write("set ylabel 'h ("+self.unitmap+")'\n")
            file.write("\n")
            file.write("range ="+str(self.size)+"\n")
            file.write("mincol ="+str(self.mincolor)+"\n")
            file.write("maxcol ="+str(self.maxcolor)+"\n")
            if self.color == "AMR":
                file.write("pascol = 1\n")         # 0.5 pour tout sauf AMR ou 1 c'est mieux mais bon...
            else: 
                file.write("pascol = 0.5\n")         # 0.5 pour tout sauf AMR ou 1 c'est mieux mais bon...
            file.write("minv ="+str(self.minvect)+"\n")
            file.write("maxv ="+str(self.maxvect)+"\n")
            file.write("set xrange [0:range]\n")
            file.write("set yrange [-range/2:range/2]\n")
            file.write("set palette maxcolors 100\n")
            file.write("\n")
            file.write("\n")
            file.write("#set term png enh size 1800,1500 19\n")
            file.write("set term post enh color 17\n")
            file.write("set output '"+self.dirname+".ps'\n")
#Multiplot
    #Plandisk
            file.write("set multiplot\n")
            #file.write("set xtics offset screen 0.0,0.02\n")
            file.write("set grid noxtics noytics noztics front\n")
            file.write("set size ratio 1\n")
            file.write("set lmargin at screen 0.05\n")
            file.write("#set tmargin at screen 0.95\n")
            file.write("set bmargin at screen 0.355\n")
            file.write("set rmargin at screen 0.50\n")
            if isvarlog.get(var[self.icolor]):
                file.write("set logscale cb\n")
                file.write('set format cb "{%L}"\n')
            else:
                file.write("unset logscale cb\n")
                file.write('set format cb "%1.1f"\n')
            file.write("set cbrange [mincol:maxcol]\n")
            file.write("set colorbox horiz user origin 0.10,0.15 size 0.35,0.02\n")
            file.write("set cblabel '"+self.namecolor+"' rotate by 0 offset screen 0, screen 0.14 \n")
            file.write("set cbtics offset screen -0.0,screen 0.005\n")
            file.write("splot 'planplan.dat' index 1 u 2:1:(($4/$3)) w image not\n")
            file.write("set cbrange [0:*]\n")
            file.write("set contour\n")
            if isvarlog.get(var[self.icolor]):
                file.write("set cntrparam levels incremental log10(mincol),pascol,log10(maxcol)\n")
            else:
                file.write("set cntrparam levels incremental mincol,pascol,maxcol\n")
            file.write("unset surface\n")
            file.write("unset clabel\n")
            if isvarlog.get(var[self.icolor]):
                file.write("splot 'planplan.dat' index 1 u 2:1:(log10(abs($4/$3))) w l lt 1 lc -1 lw 2 not\n")
            else:
                file.write("splot 'planplan.dat' index 1 u 2:1:((abs($4/$3))) w l lt 1 lc -1 lw 2 not\n")
            file.write("set surface\n")
            file.write("unset contour\n")
            file.write("set cbtics in\n")
            file.write("set xlabel ''\n")
            file.write("set ylabel ''\n")
            file.write("set format x ""\n")
            file.write("set format y ""\n")
            if isvarlog.get(var[self.ivect]):
                file.write('set format cb "{%L}"\n')
                file.write("set logscale cb\n")
            else:
                file.write("unset logscale cb\n")
                file.write('set format cb "%1.1f"\n')
            file.write("set origin 0,0\n")
            file.write("set size 1,1\n")
            file.write('set palette model RGB defined (0. "white", 1.0 "black",1.01 "blue", 4. "green")\n')
            #if self.vect == "B":      
            if self.btheta == True and self.vect != "none":   
                file.write("set palette maxcolors 13\n")
                file.write("set cbrange [9.e-2:1.e0]\n")
                file.write('set cblabel "log(|B|_{/Symbol Q}/||B||)" rotate by 90 offset screen -0.,screen 0.14\n')
                plotloc = "splot 'planplan.dat' every 10:10 index 1 u 2:1:($8/$3):($5/$7*norm):($6/$7*norm):(0) w vectors nohead filled lw 2.5 palette not"
                if self.vect == "J":
                    file.write("set cbrange [minv:maxv]\n")
                    file.write('set cblabel "log(|J|/|J|_max)" rotate by 90 offset screen -0.,screen 0.14\n')
                    file.write('set logscale cb\n')
                    plotloc = "splot 'planplan.dat' every 10:10 index 1 u 2:1:($8/$3):($5/$7*norm):($6/$7*norm):(0) w vectors filled  lw 1.5 palette not"
            elif self.btheta == False and self.vect != "none":
                file.write("set palette maxcolors 25\n")
                file.write("set cblabel '"+self.namevect+"' rotate by 90 offset screen -0.,screen 0.14\n")
                file.write("set cbrange [minv:maxv]\n")
                #normloc = "$7" # norm du vecteur total
                plotloc = "splot 'planplan.dat' every 12:12 index 1 u 2:1:($7/$3):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not"
            else:
                plotloc = ""
            file.write("set colorbox horiz user origin scree 0.55,0.15 size 0.35,0.02\n")
            file.write("norm=1.0 # range/60\n")
            file.write("norm=range/25\n")
            file.write("\n")
            file.write('#set label "a" at graph 0.07, graph 0.91 front textcolor rgb "black" font ",35"\n')
            file.write("\n")
            file.write(plotloc+"\n")
#            file.write("splot 'planplan.dat' every 8:8 index 1 u 2:1:("+normloc+"/$3):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not\n")
            #if isvarlog.get(var[self.ivect]):
                #file.write("splot 'planplan.dat' every 14:14 index 1 u 2:1:(log10($7/$3)):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not\n")
            #else:
                #file.write("splot 'planplan.dat' every 14:14 index 1 u 2:1:($7/$3):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not\n")
    #Ortho
            file.write("reset\n")
            file.write("\n")
            file.write("range2=range/2.\n")
            file.write("\n")
            file.write("set yrange [-range2:range2]\n")
            file.write("set xrange [-range2:range2]\n")
            file.write("\n")
            if self.color == "Plasma" or self.color == "vtheta":
              file.write('set palette model RGB defined (-0.5 "violet", -0.3 "blue", -0.05 "cyan", 0 "white", 0.05 "yellow", 0.3 "orange", 0.5 "red")\n')
            else:
              file.write('set palette rgbformulae 22,13,-31\n')
            file.write("set palette maxcolors 100\n")
            file.write("\n")
            file.write("set size ratio 1\n")
            file.write("set view map\n")
            file.write("\n")
            file.write("set xlabel 'R ("+self.unitmap+")' offset screen 0.0, 0.02\n")
            file.write("\n")
            file.write("#set tmargin at screen 0.95\n")
            file.write("set bmargin at screen 0.355\n")
            file.write("set lmargin at screen 0.50\n")
            file.write("#set rmargin at screen 0.99\n")
            file.write("\n")
            file.write("set grid noxtics noy2tics noytics noztics front\n")
            file.write("\n")
            file.write("unset colorbox\n")
            file.write("\n")
            file.write("set ytics offset screen 0.485,0. \n")
            file.write("set xtics offset screen 0.0,0.02\n")
            file.write("#set ytics ('-60' -60, '-40' -40, '-20' -20, '0' 0, '20' 20, '40' 40, '60' 60) offset screen 0.48,0. left\n")
            file.write("#set xtics ('-60' -60,'-40' -40,'-20' -20, '0' 0, '20' 20, '40' 40, '60' 60) offset screen 0.0,0.01\n")
            file.write("\n")
            file.write("set format y ""\n")
            file.write("\n")
            file.write("#set origin 0,0\n")
            file.write("#set size ratio 1\n")
            if isvarlog.get(var[self.icolor]):
                file.write("set logscale cb\n")
            else:
                file.write("unset logscale cb\n")
            file.write("set cbrange [mincol:maxcol]\n")
            file.write('set format cb "{%L}"\n')
            file.write("splot 'planortho.dat' index 1 u 2:1:(($4/$3)) w image not\n")
            file.write("\n")
            file.write("set cbrange [0:*]\n")
            file.write("set contour\n")
            if isvarlog.get(var[self.icolor]):
                file.write("set cntrparam levels incremental log10(mincol),pascol,log10(maxcol)\n")
            else:
                file.write("set cntrparam levels incremental mincol,pascol,maxcol\n")
            file.write("unset surface\n")
            file.write("unset clabel\n")
            if isvarlog.get(var[self.icolor]):
                file.write("splot 'planortho.dat' index 1 u 2:1:(log10(abs($4/$3))) w l lt 1 lc -1 lw 2 not\n")
            else:
                file.write("splot 'planortho.dat' index 1 u 2:1:((abs($4/$3))) w l lt 1 lc -1 lw 2 not\n")
            file.write("\n")
            file.write("set surface\n")
            file.write("unset contour\n")
            file.write("\n")
            file.write("set xlabel ''\n")
            file.write("set ylabel ''\n")
            file.write("set format x ""\n")
            file.write("set format y ""\n")
            file.write('set format cb "%1.1f"\n')
            file.write("set origin 0,0\n")
            file.write("set size 1,1\n")
            file.write('set palette model RGB defined (0. "white", 1.0 "black",1.01 "blue", 4. "green")\n')
            if isvarlog.get(var[self.ivect]):
                file.write("set logscale cb\n")
            else:
                file.write("unset logscale cb\n")
            file.write("set cbrange [minv:maxv]\n")
            file.write("norm=1.0 # range/60\n")
            file.write("norm=range/17\n")
            file.write("\n")
            file.write('set label 'b' at graph 0.07, graph 0.91 front textcolor rgb "black" font ",35"\n')
            file.write("\n")
            if self.vect != "B" and self.vect != "none":
                file.write("splot 'planortho.dat' every 8:8 index 1 u 2:1:($7/$3):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not\n")
            file.write("\n")
            file.write("unset multiplot\n")
            
            #file.write(" \n")
#
            #file.write("set contour\n")
            #file.write("set cntrparam levels discrete 0.5\n")
            #file.write("unset surface\n")
            #file.write("unset clabel\n")
            #file.write("splot 'histo.txt' index 1 u 1:2:3 w image not, 'histo.txt' index 1 u 1:2:(($3)) w l lw 2 lc 0 not\n")
            ##file.write("splot 'brho.txt' index 1 u 1:2:3 w image not, 'brho.txt' index 1 u 1:2:(($3)) w l lw 2 lc 0 not\n")
            #file.write("unset label\n")
            file.close
        except:
            print('Something went wrong in writing .gp file! Can\'t tell what?')
            exit(0)
        self.namegp='plandisk_'+self.dirname+'.gp' 
        print(self.namegp+' file created ! ') 
        #print('plandisk_'+self.dirname+'.gp file created ! ') 

    def generateshmap(self):
        print('Creating new sh files .... ') 
        name = self.dirname
        ntype = str(self.icolor)+' '+str(self.ivect)
        #print ntype
        try:
            file = open('script_'+self.dirname+'.sh','w')
            file.write("#!/bin/bash\n")
            file.write("i=0\n")
            file.write("mkdir movies\n")
            file.write("dirmovie='movies/plandisk_"+self.dirname+"'\n")
            file.write("mkdir $dirmovie\n")
            file.write('dirpwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"\n')
            file.write('dirvisu="'+self.dirvisu+'"\n')
            file.write("dir=.\n")
            file.write("psname='"+self.dirname+"'\n")
            try:
                number = '%.5d' % int(self.output)
                print(number)
            except:
                file.write("for file in $dir/output*\n")
            else:
                file.write("for file in $dir/output_"+number+"\n")
            #finally:
                #file.write("for file in $dir/output*\n")
            file.write("do\n")
            if self.output == "last":
                file.write("   echo $file\n")
                file.write("done\n")
            file.write("   let i=$i+1\n")
            file.write('   z="$(echo $file | rev |cut -d\'_\' -f1 | rev)"\n')
            file.write('   filee="$(echo $file | rev |cut -d\'/\' -f1 | rev)"\n')
            if self.erase == False:
                espace='   '
                file.write("   if [ -f $dirmovie'/'$psname'_'$z'.ps' ]\n")
                file.write("   then\n")
                file.write("      echo file $file already processed\n")
                file.write("   else\n")
            elif self.erase == True:
                espace=''
            file.write(espace+"   echo processing file $file ....\n")
            file.write(espace+'   echo "0.5 0.5 0.5" > center.dat\n')
            file.write(espace+'   echo "0 0 1 0 0 1 0 0 1 0 0 1" > angmom.dat\n')
            file.write(espace+"   echo -e 'inp' "'$filee'"'\\n''out cube.dat''\\n''direction 4''\\n''xmin 0.5''\\n''xmax 0.55''\\n''ymin 0.5''\\n''ymax 0.55''\\n''zmin 0.5''\\n''zmax 0.55\\nlmax 0\\nfile vtk\\ntype "+str(ntype)+"\\nfenetre "+str(self.zoom)+' '+str(self.size)+"\\ncenter 0.5 0.5 0.5 ")
            if self.sinks == False:
                file.write("\\nnorm "+str(self.waist)+"\\nold 1\\neos 1\\nsinks 0' > brho.dat\n")
            else:
                file.write("\\nnorm "+str(self.waist)+"\\nold 1\\neos 1\\nsinks 1' > brho.dat\n")
            if self.findcenter == True:
                file.write(espace+"   if [ $z -ne 00001 ]\n")
                file.write(espace+"   then\n")
                file.write(espace+"      $dirvisu'/findcenter'\n")
                file.write(espace+"   fi\n")
                file.write(espace+"   $dirvisu'/angmom'\n")
                file.write(espace+"   $dirvisu'/plandisk'\n")
                file.write(espace+'   mv "plandisk_${z}.txt" planplan.dat\n')
                file.write(espace+'   mv "planortho_${z}.txt" planortho.dat\n')
                file.write(espace+'   echo "load \'plandisk_$psname.gp\'" | gnuplot\n')
                file.write(espace+"   cp $psname.ps $dirmovie'/'$psname'_'$z'.ps'\n")
            elif self.sinks == True:
                file.write(espace+"   $dirvisu'/findcenter_sink'\n")
                file.write(espace+"   nline=1\n")
                file.write(espace+"   cat center_sink.dat | while read line\n")
                file.write(espace+"   do\n")
                espace2=espace+str('   ')
                file.write(espace2+"   rm -f center.dat\n")
                file.write(espace2+'   sed -n "${nline}p" center_sink.dat > center.dat \n')
                file.write(espace2+"   $dirvisu'/angmom'\n")
                file.write(espace+"   $dirvisu'/plandisk'\n")
                file.write(espace2+'   mv "plandisk_${z}.txt" planplan.dat\n')
                file.write(espace2+'   mv "planortho_${z}.txt" planortho.dat\n')
                file.write(espace2+'   echo "load \'plandisk_$psname.gp\'" | gnuplot\n')
                file.write(espace2+"   cp $psname.ps $dirmovie'/'$psname'_'$z'_sink_'$nline'.ps'\n")
                file.write(espace2+"   let nline=$nline+1\n")
                file.write(espace+"   done\n")
            else:
                file.write(espace+"   $dirvisu'/angmom'\n")
                file.write(espace+"   $dirvisu'/plandisk'\n")
                file.write(espace+'   mv "plandisk_${z}.txt" planplan.dat\n')
                file.write(espace+'   mv "planortho_${z}.txt" planortho.dat\n')
                file.write(espace+'   echo "load \'plandisk_$psname.gp\'" | gnuplot\n')
                file.write(espace+"   cp $psname.ps $dirmovie'/'$psname'_'$z'.ps'\n")
            #file.write(espace+"   $dirvisu'/angmom'\n")
            #file.write(espace+"   $dirvisu'/plandisk'\n")
            #file.write(espace+'   mv "plandisk_${z}.txt" planplan.dat\n')
            #file.write(espace+'   mv "planortho_${z}.txt" planortho.dat\n')
            #file.write(espace+'   echo "load \'plandisk_$psname.gp\'" | gnuplot\n')
            #file.write(espace+"   cp $psname.ps $dirmovie'/'$psname'_'$z'.ps'\n")
            if self.erase == False:
                file.write("   fi\n")
            file.write("done\n")
            file.close
            os.chmod('script_'+self.dirname+'.sh',0755)
        except:
            print('Something went wrong in writing .sh file! Can\'t tell what?')
            exit(0)
        self.namesh = 'script_'+self.dirname+'.sh'
        print('\n------------------------------         ./'+self.namesh+'      created !  ---------------------------- ') 
        #print('------------------------------         ./script_'+self.dirname+'.sh      created !  ---------------------------- ') 











######################################################               ######################################################
######################################################               ######################################################
######################################################   HISTOGRAM   ######################################################
######################################################               ######################################################
######################################################               ######################################################
class Histogram:
    def __init__(self):
        self.generateaxx()
        if self.x != "J":
            self.generatemaxx()
        else:
            self.minx=1.e-10
            self.maxx=1.e30
        self.generateaxy()
        if self.y != "J":
            self.generatemaxy()
        else:
            self.miny=1.e-10
            self.maxy=1.e30
        #if self.x == "rho" and self.y == "T":
            #findcenter = raw_input("Track "+str(self.y)+"("+str(self.x)+") ? (y/n default is no. If track, all output will be processed.) \n>")
            #if findcenter == "y":
                #self.rhotcenter=True
            #else:
                #self.rhotcenter=False
        #else:
            #self.rhotcenter=False
        findrhocenter = raw_input("Track "+str(self.y)+"("+str(self.x)+"_max) ? (y/n default is no. If track, all output will be processed.) \n>")
        if findrhocenter == "y":
            self.rhotcenter=True
        else:
            self.rhotcenter=False
        findcenter=raw_input("Find max density and use as center ? (y/n default is yes) \n>")
        if findcenter == "n":
            self.findcenter=False
        else:
            self.findcenter=True
        vecteur=raw_input("Probe a direction ? (default is angular momentum direction, enter 'x' for x-direction (or y, z) or type anything to generate a vector) \n>")
        if vecteur == "":
            self.vecteur= False
        elif vecteur == "x":
            self.vecteur=vector(1,0,0)
        elif vecteur == "y":
            self.vecteur=vector(0,1,0)
        elif vecteur == "z":
            self.vecteur=vector(0,0,1)
        else:
            self.generatevector()
        while True:
            if self.rhotcenter == True :
                self.output="all"
                break
            else:
                output=raw_input("Process all files (all) or select an output (number). Last output by default. \n>")
                if output=="all":
                    self.output="all"
                    print("all outputs will be processed")
                    break
                elif output=="":
                    self.output="last"
                    print("last output processed")
                    break
                else:
                    try:
                        self.output=int(output)
                        print("processing output_%.5d" % int(output))
                        break
                    except ValueError:
                        print("enter a valid number or 'all' or hit return for last output")
        if self.rhotcenter == True :
            self.erase= True
            print('Override of plots mandatory for tracking of central value ---- Overriding')
        else:
            erase=raw_input("Erase existing plots ? (y/n default is yes) \n>")
            if erase == "n":
                self.erase=False
                #if self.rhotcenter == True:
                    #self.erase=True
                    #print('Override mandatory for tracking of central temperature ---- Overriding')
            else:
                self.erase=True
        self.dirname=self.y+self.x
        self.dirvisu=dirvisu
        self.generategp()
        self.generatesh()

    def generatevector(self):
        print("Enter the coordinates of the direction vector (it will renormalize by itself):\n")
        try:
            xx=float(raw_input("x ? \n>"))
            yy=float(raw_input("y ? \n>"))
            zz=float(raw_input("z ? \n>"))
            self.vecteur=vector(xx,yy,zz)
            return 
        except ValueError:
            print 'rate, essaie encore'
            self.generatevector()

    def generateaxx(self):
        print "Histogram generation: abscissa"
        self.x = raw_input("x? ")
        try: 
            self.xnum = var.index(self.x)
        except ValueError:
            print "%s Not in variables list, try again." % self.x
            print "Reminder of variables: ",var,"\n"
            self.generateaxx()
        self.unitx = unit[self.xnum]
        self.namex = varnamegp[self.xnum]
    def generatemaxx(self):
        self.minx=Default(self.x).m
        self.maxx=Default(self.x).M
        try:
            self.minx = float(raw_input("Min value for %s ? Default is %s (in %s)\n>" % (self.x,self.minx,self.unitx) ))
        except ValueError:
            print 'Default used for minimum %s' % self.x
        try:
            self.maxx = float(raw_input("Max value for %s ? Default is %s (in %s) \n>" % (self.x,self.maxx,self.unitx) ))
        except ValueError:
            print 'Default value used for maximum %s' % self.x
        return

    def generateaxy(self):
        print "Histogram generation: Y-axis"
        self.y = raw_input("y? ")
        try: 
            self.ynum = var.index(self.y)
            #return
        except ValueError:
            print "%s Not in variables list, try again." % self.y
            print "Reminder of variables: ",var,"\n"
            self.generateaxy()
        self.unity = unit[self.ynum]
        self.namey = varnamegp[self.ynum]
    def generatemaxy(self):
        self.miny=Default(self.y).m
        self.maxy=Default(self.y).M
        try:
            self.miny = float(raw_input("Min value for %s ? Default is %s (in %s)\n>" % (self.y,self.miny,self.unity) ))
        except ValueError:
            print 'Default used for minimum %s' % self.y
        try:
            self.maxy = float(raw_input("Max value for %s ? Default is %s (in %s)\n>" % (self.y,self.maxy,self.unity) ))
        except ValueError:
            print 'Default value used for maximum %s' % self.y
        return

    def generategp(self):
        print('Creating new gp files .... ') 
        name = self.dirname
        xd = (self.maxx-self.minx)/float(10)
        yd = (self.maxy-self.miny)/float(10)
        try:
            file = open('plothisto_'+self.dirname+'.gp','w')
            file.write("set view map\n")
            file.write("set logscale cb\n")
            file.write('set palette model RGB defined (0.9 "white", 1. "white", 1 "red", 2 "red", 2 "black", 3 "black", 3 "white", 100 "green")\n')
            file.write("\n")
            file.write('t=system("awk \'/t =/ \{print $3\}\' histo.txt")\n')
            file.write('t=t+0.0\n')
            file.write('newlab=sprintf("time = %4.3f Kyears",t)\n')
            file.write("\n")
            #file.write("set xlabel 'log({/Symbol r}) (g cm^{-3})'\n")
            file.write("set xlabel '"+self.namex+"'\n")
            #file.write("set xlabel 'log("+self.x+")'\n")
            file.write("set ylabel '"+self.namey+"'\n")
            #file.write("set ylabel 'log("+self.y+")'\n")
            file.write("\n")
            if isvarlog.get(var[self.ynum]):
                #file.write("set xrange ["+str(math.log10(self.minx))+":"+str(math.log10(self.maxx+xd))+"]\n")
                file.write("set yrange ["+str(math.log10(self.miny))+":"+str(math.log10(self.maxy+yd))+"]\n")
                plotvary = str("($2)")
                plotvarycenter = str("(log10($3))")
            else:
                #file.write("set xrange ["+str((self.minx))+":"+str((self.maxx+xd))+"]\n")
                file.write("set yrange ["+str((self.miny))+":"+str((self.maxy+yd))+"]\n")
                plotvary = str("(10**($2))")
                plotvarycenter = str("(($3))")
            file.write("set cbrange [0.9:100]\n")
            file.write("set palette maxcolors 100\n")
            file.write("\n")
            file.write("set grid noxtics noytics noztics front\n")
            file.write("\n")
            file.write("unset colorbox\n")
            if self.vecteur == False:
                direction = "J"
            else:
                direction = str(self.vecteur.x)+' '+str(self.vecteur.y)+' '+str(self.vecteur.z)
            file.write("set contour\n")
            file.write("set cntrparam levels discrete 0.5\n")
            file.write("#unset surface\n")
            file.write("unset clabel\n")
            file.write("set term post enh color 17\n")
            file.write("set output '"+self.dirname+".ps'\n")
            if self.rhotcenter == True:
                os.system("rm XY_centre.dat")
                #file.write("splot 'histo.txt' index 1 u 1:"+plotvary+":(($3)) w l lw 2 lc -1 not, 'probe.dat' every 10 u 1:"+plotvary+":(0.96) w lp lt 2 lw 5 ps 2 pt 1 palette not, 'XY_centre.dat' u (log10($2)):"+plotvarycenter+":(0.99) w lp pt 6 lt 1 lw 4 ps 2 palette notit, '' u (NaN):(NaN):(NaN) w lp pt 6 lt 1 lw 4 ps 2 lc rgb 'black' tit 'Centre track', '' u (NaN):(NaN):(NaN) w lp lt 2 lw 5 pt 1 ps 2 lc rgb 'red' tit  'direction "+direction+"'\n")
                file.write("splot 'histo.txt' index 1 u 1:"+plotvary+":(($3)) w image not\n")
            else:
              #file.write("splot 'histo.txt' index 1 u 1:"+plotvary+":(($3)) w l lw 2 lc -1 not, 'probe.dat' every 10 u 1:"+plotvary+":(0.96) w lp lt 2 lw 5 ps 2 pt 1 palette not, '' u (NaN):(NaN):(NaN) w lp lt 2 lw 5 pt 1 ps 2 lc rgb 'red' tit  'direction "+direction+"'\n")
                file.write("splot 'histo.txt' index 1 u 1:"+plotvary+":(($3)) w image not\n")
            #file.write("splot 'brho.txt' index 1 u 1:2:3 w image not, 'brho.txt' index 1 u 1:2:(($3)) w l lw 2 lc 0 not\n")
            file.write("unset label\n")
            file.close
        except:
            print('Something went wrong in writing .gp file! Can\'t tell what?')
            exit(0)
        self.namegp = 'plothisto_'+self.dirname+'.gp'
        print(self.namegp+' file created ! ') 

######################################################## sh
    def generatesh(self):
        print('Creating new sh files .... ') 
        name = self.dirname
        ntype = str(self.ynum)+' '+str(self.xnum)
        #print ntype
        try:
            file = open('script_'+self.dirname+'.sh','w')
            file.write("#!/bin/bash\n")
            file.write("i=0\n")
            if self.rhotcenter == True:
                file.write("rm XY_centre.dat\n")
            file.write("mkdir movies\n")
            file.write("dirmovie='movies/histo_"+self.dirname+"'\n")
            file.write("mkdir $dirmovie\n")
            file.write('dirpwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"\n')
            file.write('dirvisu="'+self.dirvisu+'"\n')
            file.write("dir=.\n")
            file.write("psname='"+self.dirname+"'\n")
            try:
                number = '%.5d' % int(self.output)
                print(number)
            except:
                file.write("for file in $dir/output*\n")
            else:
                file.write("for file in $dir/output_"+number+"\n")
            #finally:
                #file.write("for file in $dir/output*\n")
            file.write("do\n")
            if self.output == "last":
                file.write("   echo $file\n")
                file.write("done\n")
            file.write("   let i=$i+1\n")
            file.write('   z="$(echo $file | rev |cut -d\'_\' -f1 | rev)"\n')
            file.write('   filee="$(echo $file | rev |cut -d\'/\' -f1 | rev)"\n')
            if self.erase == False:
                espace='   '
                file.write("   if [ -f $dirmovie'/'$psname'_'$z'.ps' ]\n")
                file.write("   then\n")
                file.write("      echo file $file already processed\n")
                file.write("   else\n")
            elif self.erase == True:
                espace=''
            file.write(espace+"   echo processing file $file ....\n")
            file.write(espace+'   echo "0.5 0.5 0.5" > center.dat\n')
            file.write(espace+'   echo "0 0 1 0 0 1 0 0 1 0 0 1" > angmom.dat\n')
            file.write(espace+"   echo -e 'inp' "'$filee'"'\\n''out cube.dat''\\n''direction 4''\\n''xmin 0.5''\\n''xmax 0.55''\\n''ymin 0.5''\\n''ymax 0.55''\\n''zmin 0.5''\\n''zmax 0.55\\nlmax 0\\nfile vtk\\ntype "+str(ntype)+"\\nfenetre 1. 10000000000000\\ncenter 0.5 0.5 0.5\\nhisto ")
            #minmax=str(self.minx)+' '+str(self.miny)
            file.write(str(self.minx)+' '+str(self.miny)+' '+str(self.maxx)+' '+str(self.maxy))
            file.write("\\nvector ")
            if self.vecteur == False:
                file.write('0 0 0')
            else:
                file.write(str(self.vecteur.x)+' '+str(self.vecteur.y)+' '+str(self.vecteur.z))
            file.write("\\nnorm 10000\\nold 1\\neos 1' > brho.dat\n")
            if self.findcenter == True:
                file.write(espace+"   if [ $z -ne 00001 ]\n")
                file.write(espace+"   then\n")
                file.write(espace+"      $dirvisu'/findcenter'\n")
                file.write(espace+"   fi\n")
            if self.vecteur == False:
                file.write(espace+"   $dirvisu'/angmom'\n")
            file.write(espace+"   $dirvisu'/histo'\n")
            file.write(espace+'   echo "load \'plothisto_$psname.gp\'" | gnuplot\n')
            file.write(espace+"   cp $psname'.ps' $dirmovie'/'$psname'_'$z'.ps'\n")
            if self.erase == False:
                file.write("   fi\n")
            file.write("done\n")
            file.close
            os.chmod('script_'+self.dirname+'.sh',0755)
        except:
            print('Something went wrong in writing .sh file! Can\'t tell what?')
            exit(0)
        self.namesh = 'script_'+self.dirname+'.sh'
        print('\n------------------------------         ./'+self.namesh+'      created !  ---------------------------- ') 
        #print('------------------------------         ./script_'+self.dirname+'.sh      created !  ---------------------------- ') 




######################################################              ######################################################
######################################################              ######################################################
######################################################     SLICE    ######################################################
######################################################              ######################################################

class Slice:
    def __init__(self):

        self.genesize()
        self.genezoom()
        self.genemap()
        self.genemax()

        direct = raw_input("Slice plane (x,y,z), default is z (disk plane)\n>")
        if direct == "x":
           self.direct='x'
        elif direct == "y":
           self.direct='y'
        elif direct == "z":
           self.direct='z'
        else:
           self.direct='z'

        ###############################    findcenter    ########################
        sinks = raw_input("Do you use sinks ? (y/n default is no.) \n>")
        if sinks == "y":
            self.sinks=True
            self.findcenter=False
        else:
            self.sinks=False
            self.findcenter=True 

        findcenter=raw_input("Find max density and use as center ? (y/n default is yes) \n>")
        if findcenter == "n":
            self.findcenter=False
        else:
            self.findcenter=True

        while True:
            output=raw_input("Process all files (all) or select an output (number). Last output by default. \n>")
            if output=="all":
                self.output="all"
                print("all outputs will be processed")
                break
            elif output=="":
                self.output="last"
                print("processing last output")
                break
            else:
                try:
                    self.output=int(output)
                    print("processing output_%.5d" % int(output))
                    break
                except ValueError:
                    print("enter a valid number, or 'all', or hit enter for last output...\n")

        ###############################    erase    ########################
        erase=raw_input("Erase existing plots ? (y/n default is yes) \n>")
        if erase == "n":
            self.erase=False
            #if self.rhotcenter == True:
                #self.erase=True
                #print('Override mandatory for tracking of central temperature ---- Overriding')
        else:
            self.erase=True

        #self.dirname="map"+self.color+"_vect"+self.vect
        self.dirname=self.color+self.vect
        self.dirvisu=dirvisu
        self.generategpmap()
        self.generateshmap()

    def generatevector(self):
        print("Enter the coordinates of the direction vector (it will renormalize by itself):\n")
        try:
            xx=float(raw_input("x ? \n>"))
            yy=float(raw_input("y ? \n>"))
            zz=float(raw_input("z ? \n>"))
            self.vecteur=vector(xx,yy,zz)
            return 
        except ValueError:
            print 'rate, essaie encore'
            self.generatevector()

    def genesize(self):
        print "Size generation"
        unitmap = "a.u."
        while True:
            size = raw_input("Size of the window (in %s) ? " % unitmap)
            try: 
                self.size = float(size)
                break
            except ValueError:
                print "%s Not a valid size, try again.\n" % size
                #self.genesize()
        self.unitmap = unitmap
        print("Window of %s (in %s)\n" % (self.size,self.unitmap))
        
    def genezoom(self):
        print "Resolution (number of points per window)"
        fenetredefault = 200
        while True:
            zoom = raw_input("Resolution ? (default is %s)" % fenetredefault)
            if zoom == '':
                self.zoom=fenetredefault
                break
            else:
                try: 
                    self.zoom = float(zoom)
                    break
                except ValueError:
                    print "%s Not a valid zoom, try again.\n" % zoom
                    #self.genesize()
        #print("Zooming in: 2^%s\n" % (self.zoom))
        
    
    def genemap(self):
        print "Variables generation"
        #Colormap
        while True:
            try: 
                self.color = raw_input("colormap variable ? ")
                self.icolor = var.index(self.color)
                break
            except ValueError:
                print "%s Not in variables list, try again." % self.color
                print "Reminder of variables: ",var,"\n"
        self.unitcolor = unit[self.icolor]
        self.namecolor = varnamegp[self.icolor]
        #Vectorfiled
        while True:
            try: 
                self.vect = raw_input("vectorfield variable ? ")
                self.ivect = var.index(self.vect)
                wrongvar = varvect.index(self.vect)
                break
            except ValueError:
                print "%s Not in variables list, try again." % self.vect
                print "Reminder of variables: ",varvect,"\n"
        self.btheta = False
        if self.vect == "B":
            thetastring = u'\u0398'.encode('utf-8')
            btheta = raw_input("Vector field is by default |B_"+thetastring+"|/||B||. type 'n' to use ||B|| as color for the vector field.\n")
            if btheta != "n":
                self.btheta = True
        if self.vect == "J" or self.vect == "none":
            self.btheta = True
        self.unitvect = unit[self.ivect]
        self.namevect = varnamegp[self.ivect]


    def genemax(self):
        #colormap
        self.mincolor=Default(self.color).m
        self.maxcolor=Default(self.color).M
        try:
            self.mincolor = float(raw_input("Min value for %s ? Default is %s (in %s)\n>" % (self.color,self.mincolor,self.unitcolor) ))
        except ValueError:
            print 'Default used for minimum %s' % self.color
        try:
            self.maxcolor = float(raw_input("Max value for %s ? Default is %s (in %s)\n>" % (self.color,self.maxcolor,self.unitcolor) ))
        except ValueError:
            print 'Default value used for maximum %s' % self.color
        #vectorfield
        self.minvect=Default(self.vect).m
        self.maxvect=Default(self.vect).M
        if self.btheta == False:
            try:
                self.minvect = float(raw_input("Min value for %s ? Default is %s (in %s)\n>" % (self.vect,self.minvect,self.unitvect) ))
            except ValueError:
                print 'Default used for minimum %s' % self.vect
            try:
                self.maxvect = float(raw_input("Max value for %s ? Default is %s (in %s)\n>" % (self.vect,self.maxvect,self.unitvect) ))
            except ValueError:
                print 'Default value used for maximum %s' % self.vect
        return

    def generategpmap(self):
        print('Creating new gp files .... ') 
        name = self.dirname
        try:
            file = open('plancoup_'+self.dirname+'.gp','w')
            file.write("set view map\n")
            #file.write("set logscale cb\n")
            if self.color == "Plasma" or self.color == "vtheta":
              file.write('set palette model RGB defined (-0.5 "violet", -0.3 "blue", -0.05 "cyan", 0 "white", 0.05 "yellow", 0.3 "orange", 0.5 "red")\n')
            else:
              file.write('set palette rgbformulae 22,13,-31\n')
            file.write("\n")

            file.write('t=system("awk \'/t =/ \{print $3\}\' br.txt")\n')
            file.write('t=t+0.0\n')
            file.write('newlab=sprintf("time = %4.3f Kyears",t)\n')
            file.write('set label front newlab at screen 0.45, screen 0.98\n')
            file.write('\n')
#file.write("msink = `echo $(awk '{print $6}' center.dat)`\n")
#file.write("vx = `echo $(awk '{print $9}' center.dat)`\n")
#file.write("vy = `echo $(awk '{print $12}' center.dat)`\n")
#file.write("vz = `echo $(awk '{print $15}' center.dat)`\n")
#file.write("print msink\n")
#file.write("print vx, vy,vz\n")
            file.write("\n")
            #file.write("set xlabel 'log({/Symbol r}) (g cm^{-3})'\n")
            #file.write("set xlabel '"+self.namex+"'\n")
            file.write("set xlabel 'R ("+self.unitmap+")'\n")
            #file.write("set ylabel '"+self.namey+"'\n")
            file.write("set ylabel 'h ("+self.unitmap+")'\n")
            file.write("\n")
            file.write("range ="+str(self.size)+"\n")
            file.write("mincol ="+str(self.mincolor)+"\n")
            file.write("maxcol ="+str(self.maxcolor)+"\n")
            if self.color == "AMR":
                file.write("pascol = 1\n")         # 0.5 pour tout sauf AMR ou 1 c'est mieux mais bon...
            else: 
                file.write("pascol = 0.5\n")         # 0.5 pour tout sauf AMR ou 1 c'est mieux mais bon...
            file.write("minv ="+str(self.minvect)+"\n")
            file.write("maxv ="+str(self.maxvect)+"\n")
            file.write("set xrange [-range/2:range/2]\n")
            file.write("set yrange [-range/2:range/2]\n")
            file.write("set palette maxcolors 100\n")
            file.write("\n")
            file.write("\n")
            file.write("#set term png enh size 1800,1500 19\n")
            file.write("set term post enh color 17\n")
            file.write("set output '"+self.dirname+".ps'\n")
#Multiplot
    #Plandisk
            file.write("set multiplot\n")
            #file.write("set xtics offset screen 0.0,0.02\n")
            file.write("set grid noxtics noytics noztics front\n")
            file.write("set size ratio 1\n")
            if isvarlog.get(var[self.icolor]):
                file.write("set logscale cb\n")
                file.write('set format cb "{%L}"\n')
            else:
                file.write("unset logscale cb\n")
                file.write('set format cb "%1.1f"\n')
            file.write("set cbrange [mincol:maxcol]\n")
            #file.write("set colorbox horiz user origin 0.10,0.15 size 0.35,0.02\n")
            file.write("set cblabel '"+self.namecolor+"'\n")
            file.write("splot 'planplan.dat' index 1 u 2:1:(($4/$3)) w image not\n")
            file.write("set cbrange [0:*]\n")
            file.write("set contour\n")
            if isvarlog.get(var[self.icolor]):
                file.write("set cntrparam levels incremental log10(mincol),pascol,log10(maxcol)\n")
            else:
                file.write("set cntrparam levels incremental mincol,pascol,maxcol\n")
            file.write("unset surface\n")
            file.write("unset clabel\n")
            if isvarlog.get(var[self.icolor]):
                file.write("splot 'planplan.dat' index 1 u 2:1:(log10(abs($4/$3))) w l lt 1 lc -1 lw 2 not\n")
            else:
                file.write("splot 'planplan.dat' index 1 u 2:1:((abs($4/$3))) w l lt 1 lc -1 lw 2 not\n")
            file.write("set surface\n")
            file.write("unset contour\n")
            file.write("set cbtics in\n")
            file.write("set xlabel ''\n")
            file.write("set ylabel ''\n")
            file.write("set format x ""\n")
            file.write("set format y ""\n")
            if isvarlog.get(var[self.ivect]):
                file.write('set format cb "{%L}"\n')
                file.write("set logscale cb\n")
            else:
                file.write("unset logscale cb\n")
                file.write('set format cb "%1.1f"\n')
            file.write("set origin 0,0\n")
            file.write("set size 1,1\n")
            file.write('set palette model RGB defined (0. "white", 1.0 "black",1.01 "blue", 4. "green")\n')
            #if self.vect == "B":      
            if self.btheta == True and self.vect != "none":   
                file.write("set palette maxcolors 13\n")
                file.write("set cbrange [9.e-2:1.e0]\n")
                file.write('set cblabel "log(|B|_{/Symbol Q}/||B||)"\n')
                plotloc = "splot 'planplan.dat' every 10:10 index 1 u 2:1:($8/$3):($5/$7*norm):($6/$7*norm):(0) w vectors nohead filled lw 2.5 palette not"
                if self.vect == "J":
                    file.write("set cbrange [minv:maxv]\n")
                    file.write('set cblabel "log(|J|/|J|_max)"\n')
                    file.write('set logscale cb\n')
                    plotloc = "splot 'planplan.dat' every 10:10 index 1 u 2:1:($8/$3):($5/$7*norm):($6/$7*norm):(0) w vectors filled  lw 1.5 palette not"
            elif self.btheta == False and self.vect != "none":
                file.write("set palette maxcolors 25\n")
                file.write("set cblabel '"+self.namevect+"'\n")
                file.write("set cbrange [minv:maxv]\n")
                #normloc = "$7" # norm du vecteur total
                plotloc = "splot 'planplan.dat' every 12:12 index 1 u 2:1:($7/$3):($5/$7*norm):($6/$7*norm):(0) w vectors filled lw 1.5 palette not"
            else:
                plotloc = ""
            file.write("set colorbox user origin scree 0.9,0.17 size 0.04,0.6\n")
            file.write("norm=1.0 # range/60\n")
            file.write("norm=range/25\n")
            file.write("\n")
            file.write('#set label "a" at graph 0.07, graph 0.91 front textcolor rgb "black" font ",35"\n')
            file.write("\n")
            file.write(plotloc+"\n")

            file.close
        except:
            print('Something went wrong in writing .gp file! Can\'t tell what?')
            exit(0)
        self.namegp='plancoup_'+self.dirname+'.gp' 
        print(self.namegp+' file created ! ') 

    def generateshmap(self):
        print('Creating new sh files .... ') 
        name = self.dirname
        ntype = str(self.icolor)+' '+str(self.ivect)
        #print ntype
        try:
            file = open('coupe_'+self.dirname+'.sh','w')
            file.write("#!/bin/bash\n")
            file.write("i=0\n")
            file.write("mkdir movies\n")
            file.write("dirmovie='movies/plancoup_"+self.dirname+"'\n")
            file.write("mkdir $dirmovie\n")
            file.write('dirpwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"\n')
            file.write('dirvisu="'+self.dirvisu+'"\n')
            file.write("dir=.\n")
            file.write("psname='"+self.dirname+"'\n")
            try:
                number = '%.5d' % int(self.output)
                print(number)
            except:
                file.write("for file in $dir/output*\n")
            else:
                file.write("for file in $dir/output_"+number+"\n")
            #finally:
                #file.write("for file in $dir/output*\n")
            file.write("do\n")
            if self.output == "last":
                file.write("   echo $file\n")
                file.write("done\n")
            file.write("   let i=$i+1\n")
            file.write('   z="$(echo $file | rev |cut -d\'_\' -f1 | rev)"\n')
            file.write('   filee="$(echo $file | rev |cut -d\'/\' -f1 | rev)"\n')
            if self.erase == False:
                espace='   '
                file.write("   if [ -f $dirmovie'/'$psname'_'$z'.ps' ]\n")
                file.write("   then\n")
                file.write("      echo file $file already processed\n")
                file.write("   else\n")
            elif self.erase == True:
                espace=''
            file.write(espace+"   echo processing file $file ....\n")
            file.write(espace+'   echo "0.5 0.5 0.5" > center.dat\n')
            file.write(espace+'   echo "0 0 1 0 0 1 0 0 1 0 0 1" > angmom.dat\n')
            file.write(espace+"   echo -e 'inp' "'$filee'"'\\n''out cube.dat''\\n''direction 4''\\n''xmin 0.5''\\n''xmax 0.55''\\n''ymin 0.5''\\n''ymax 0.55''\\n''zmin 0.5''\\n''zmax 0.55\\nlmax 0\\nfile vtk\\ntype "+str(ntype)+"\\nfenetre "+str(self.zoom)+' '+str(self.size)+"\\ncenter 0.5 0.5 0.5 ")
            if self.sinks == False:
                file.write("\\nnorm 1\\nold 1\\neos 1\\nsinks 0\\ndirect "+str(self.direct)+"' > brho.dat\n")
            else:
                file.write("\\nnorm 1\\nold 1\\neos 1\\nsinks 1\\ndirect "+str(self.direct)+"' > brho.dat\n")
            if self.findcenter == True:
                file.write(espace+"   if [ $z -ne 00001 ]\n")
                file.write(espace+"   then\n")
                file.write(espace+"      $dirvisu'/findcenter'\n")
                file.write(espace+"   fi\n")
                #file.write(espace+"   $dirvisu'/angmom'\n")
                file.write(espace+"   $dirvisu'/coupe'\n")
                file.write(espace+'   mv "plandisk_${z}.txt" planplan.dat\n')
                file.write(espace+'   echo "load \'plancoup_$psname.gp\'" | gnuplot\n')
                file.write(espace+"   cp $psname.ps $dirmovie'/'$psname'_'$z'.ps'\n")
            elif self.sinks == True:
                file.write(espace+"   $dirvisu'/findcenter_sink'\n")
                file.write(espace+"   nline=1\n")
                file.write(espace+"   cat center_sink.dat | while read line\n")
                file.write(espace+"   do\n")
                espace2=espace+str('   ')
                file.write(espace2+"   rm -f center.dat\n")
                file.write(espace2+'   sed -n "${nline}p" center_sink.dat > center.dat \n')
                #file.write(espace2+"   $dirvisu'/angmom'\n")
                file.write(espace+"   $dirvisu'/coupe'\n")
                file.write(espace2+'   mv "plandisk_${z}.txt" planplan.dat\n')
                file.write(espace2+'   echo "load \'plancoup_$psname.gp\'" | gnuplot\n')
                file.write(espace2+"   cp $psname.ps $dirmovie'/'$psname'_'$z'_sink_'$nline'.ps'\n")
                file.write(espace2+"   let nline=$nline+1\n")
                file.write(espace+"   done\n")
            else:
              #file.write(espace+"   $dirvisu'/angmom'\n")
                file.write(espace+"   $dirvisu'/coupe'\n")
                file.write(espace+'   mv "plandisk_${z}.txt" planplan.dat\n')
                file.write(espace+'   echo "load \'plancoup_$psname.gp\'" | gnuplot\n')
                file.write(espace+"   cp $psname.ps $dirmovie'/'$psname'_'$z'.ps'\n")
            #file.write(espace+"   $dirvisu'/angmom'\n")
            #file.write(espace+"   $dirvisu'/plandisk'\n")
            #file.write(espace+'   mv "plandisk_${z}.txt" planplan.dat\n')
            #file.write(espace+'   mv "planortho_${z}.txt" planortho.dat\n')
            #file.write(espace+'   echo "load \'plandisk_$psname.gp\'" | gnuplot\n')
            #file.write(espace+"   cp $psname.ps $dirmovie'/'$psname'_'$z'.ps'\n")
            if self.erase == False:
                file.write("   fi\n")
            file.write("done\n")
            file.close
            os.chmod('coupe_'+self.dirname+'.sh',0755)
        except:
            print('Something went wrong in writing .sh file! Can\'t tell what?')
            exit(0)
        self.namesh = 'coupe_'+self.dirname+'.sh'
        print('\n------------------------------         ./'+self.namesh+'      created !  ---------------------------- ') 
        #print('------------------------------         ./script_'+self.dirname+'.sh      created !  ---------------------------- ') 







######################################################
######################################################   START
######################################################
def start():
    #H = mapp()
#    H = Histogram()
#    process = raw_input("\n \nProcess the script directly ? default is yes, n for no.\n \n")
#    if process == 'non' or process == 'n' or process == 'no':
#        exit(0)
#    else:
#        os.system('./'+str(H.namesh))
#        os.system('gp '+str(H.dirname)+'.ps')
#    exit(0)
    #process = raw_input("\n \nProcess the script directly ? default is yes, n for no.\n \n")
    #if process == 'non' or process == 'n' or process == 'no':
        #exit(0)
    #else:
        #os.system('./'+str(H.namesh))
        #os.system('gp '+str(H.dirname)+'.ps')
    

    if len(sys.argv) > 1:
        choice2=str(sys.argv[1])
        choice = choice2.replace('-','')
    else:
        choice = raw_input(".h for help, or hit return to start \n> ")
    if choice == ".help" or choice == ".h":
        help()
    elif choice == "histo":
        H = Histogram()
    elif choice == "map":
        H = Mapp()
    else:
        print "Wrong choice for histo or map, try again."
        while True :
            figure2 = raw_input("histo, map or slice ?  \n> ")
            if figure2 == "histo":
                H = Histogram()
                break
            elif figure2 == "map":
                H = Mapp()
                break
            elif figure2 == "slice":
                H = Slice()
                break
            #elif figure2 == ".h" or figure2 == ".help":
                #help()
                #break
            #else:
                #figure2 = raw_input("wrong again... histo or map ?  \n> ")

    process = raw_input("\n \nProcess the script directly ? default is yes, n for no.\n \n")
    if process == 'non' or process == 'n' or process == 'no':
        exit(0)
    else:
        os.system('./'+str(H.namesh))
        os.system('gv '+str(H.dirname)+'.ps')



#################################################################
####################### starting baratin ########################
#################################################################
print "This snake is a versatile visualisation tools."
print "Help can be accessed with .h \n"
print "If the visu directory is not %s, change it in visu.py at 'dirvisu=' \n" % dirvisu


##################### CLEAN #######################
#os.system('rm -f '+dirvisu+'/angmom')
#os.system('rm -f '+dirvisu+'/plandisk')
#os.system('rm -f '+dirvisu+'/histo')
#os.system('rm -f '+dirvisu+'/findcenter')



#file = dirvisu+'/'+"histo"
#file2 = dirvisu+'/'+"histo.f90"
#print "last modified: %s" % time.ctime(os.path.getmtime(file))
#timeexec = os.path.getmtime(file)
#timef90 = os.path.getmtime(file2)
#if timef90 > timeexec:
    #print 'aieieieia'



files = ["histo","plandisk","angmom","findcenter","findcenter_sink","coupe"]
for i,item in enumerate(files):
    file = dirvisu+'/'+str(item) 
    filef90 = file+".f90"
    #print filef90, file
    isfile = os.path.isfile(file)
    if isfile==False :
        print ("Compiling "+filef90+" source file --- \n")
        os.system('gfortran -O2 '+filef90+' -o '+file)
    timeexec = os.path.getmtime(file)
    timef90 = os.path.getmtime(filef90)
    if timef90 > timeexec:
        print ("Compiling "+filef90+" source file --- \n")
        os.system('gfortran -O2 '+filef90+' -o '+file)
        
#file = dirvisu+'/'+"histo"
#isfile = os.path.isfile(file)

#isfile = os.path.isfile(dirvisu+'/'+"histo")
#if isfile==False:
    #print ("Compiling histo.f90 source file --- \n")
    #os.system('gfortran -O2 '+dirvisu+'/histo.f90 -o '+dirvisu+'/histo')
#isfile = os.path.isfile(dirvisu+'/'+"plandisk")
#
#if isfile==False:
    #print ("Compiling plandiskplan.f90 source file --- \n")
    #os.system('gfortran -O2 '+dirvisu+'/plandiskplan.f90 -o '+dirvisu+'/plandisk')
#
#isfile = os.path.isfile(dirvisu+'/'+"angmom")
#if isfile==False:
    #print ("Compiling angmom.f90 source file --- \n")
    #os.system('gfortran -O2 '+dirvisu+'/angmom.f90 -o '+dirvisu+'/angmom')
#isfile = os.path.isfile(dirvisu+'/'+"findcenter")
#if isfile==False:
    #print ("Compiling findcenterhisto.f90 source file --- \n")
    #os.system('gfortran -O2 '+dirvisu+'/findcenterhisto.f90 -o '+dirvisu+'/findcenter')

os.system("rm histo.txt ; rm probe.dat ; rm brho.dat ; rm br.txt")

isfile = os.path.isfile("tab_eos.dat")
if isfile==False:
    try:
        tmp = open(dirvisu+"/tab_eos.dat")
        tmp.close()
        print ("Copying eos data --- \n")
        os.system('cp '+dirvisu+'/tab_eos.dat .')
    except:
        print "\n\n-------------- Missing eos data file. ------------------\n"
        exit(0)

try:
    for i,item in enumerate(files):
        file = dirvisu+'/'+str(item) 
        tmp = open(file)
        tmp.close()
    tmp = open(dirvisu+'/'+"tab_eos.dat")
    tmp.close()
except:
    print "\n somefile.f90 or eos data missing, please start again with the files\n"
    sys.exit(1)

    
start()
